#!/usr/bin/env ruby

require 'curb'
require 'json'
require 'sentimental'

Sentimental.load_defaults
Sentimental.threshold = 0.1
analyzer = Sentimental.new

f = File.open('ebola_database.json')
str = ""
f.each { |line|
	str += line
}
count = 1
h = JSON.parse(str)
str = ""
h.keys().each {|key|
	json = {}

	date = h[key]["date"]
	dateGood = date.gsub("-","/")
	timestamp =  "#{dateGood} #{h[key]["time"]}"

	json[:data] = {}
	ts = timestamp.split("/")
	year = ts[0]
	month = ts[1].to_i
	dt = ts[2].split(" ")
	day = dt[0].to_i
	time = dt[1]
	if ((month == 11 && day >= 6 && year == "2014") || (month == 12))

	else
		str = month.to_s + "/" + day.to_s + "/" + year + "-" + time

		json[:data][:timestamp] = str
		json[:data][:url] = key
		
		body = h[key]["body"]
		x = body.gsub("\n", "")
		x = x.gsub("\"", "ddd")
		x = x.gsub("'","qqq")
		json[:data][:body] = x
		sentiment1 = analyzer.get_sentiment(body)
		score1 = analyzer.get_score(body)
		json[:data][:sentiment] = sentiment1
		json[:data][:score] = score1

		gener = JSON.generate(json)

		http = Curl.post("localhost:9200/news/all2/"+count.to_s, gener)
		#if (count % 100 == 0) 
		puts http.body_str
		count += 1
	end
}

