#!/usr/bin/env ruby

x = ["gold.csv", "silver.csv"]
y = ["gold_percent.csv", "silver_percent.csv"]


for j in (0..1)

	filename = x[j]
	puts filename
 
	f = File.open(filename)	
	f2 = File.open(y[j], "w")
	name = filename.split(".")[0]

	f = f.readlines
	for i in (0..f.size - 2)

		arg = f[i].split(",")
		timestamp = arg[0]
		open = arg[1]
		
		arg1 = f[i + 1].split(",")
		timestamp1 = arg1[0]
		open1 = arg1[1]
	
		f2.print timestamp
		f2.print ","
		percent_open = (((open1.to_f - open.to_f)/open.to_f)*100)
		f2.print percent_open
		
		f2.puts
	end

end
