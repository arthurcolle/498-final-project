#!/usr/bin/env ruby

require 'curb'
require 'json'
require 'time'
require 'date'

def pull_articles(things)
	i = 10001
	total = 11453
	total = 10010
	while (i < total)
		http = Curl.get('localhost:9200/news/all/'+i.to_s)
		body_str = http.body_str
		json = JSON.parse(body_str)

		source = json['_source']
		data = source['data']
		
		timestamp = data['timestamp']
		
		body = data['body']
		url = data['url']
		score = data['score']
		sentiment = data['sentiment']

		j = {}
		j[:data] = {}
		pc = j[:data][:percent_change] = {}
		
		things.each { |k,v| 
			pc[k] = {} if pc[k].nil?
			pc[k][:hours_12] = compute_change(v, 12*60, timestamp)
			pc[k][:hours_24] = compute_change(v, 24*60, timestamp)
		}
		g = JSON.generate(j)

		Curl.post('localhost:9200/news/change/'+i.to_s, g)
		i += 1
	end
end

def compute_change(filename, interval_in_minutes, date)
	interval = interval_in_minutes
	strt = date.gsub("-"," ")
	#puts "Querying " + filename + " for " + interval.to_s + " minute interval starting at " + strt
	f = File.open(filename)	
	name = filename.split(".")[0]
	f = f.readlines
	prices = {}

	start = Time.strptime(f[1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i
	en = Time.strptime(f[f.size-1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

	requested = Time.strptime(strt, "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

	ln_ct = 0
	start.step(en, 60) do |i|
		prices[i] = f[ln_ct]
		ln_ct = ln_ct + 1
	end

	open_start = prices[requested].split(",")[1].to_f
	open_end = prices[requested + (60*interval.to_i)].split(",")[1].to_f
	# puts "Price at start: " + open_start.to_s
	# puts "Price at end: " + open_end.to_s
	(((open_end.to_f - open_start.to_f)/open_start.to_f)*100)
end

things = {
	:eurodollar => "EURUSD_minutely.csv",
	:eza => "eza_minutely.csv",
	:poundyen => "GBPJPY_minutely.csv",
	:gold => "gold_minutely.csv",
	:nge => "nge_minutely.csv",
	:silver => "silver_minutely.csv",
	:spy => "spy_minutely.csv",
	:tmkr => "tkmr_minutely.csv",
	:dollaryen => "USDJPY_minutely.csv"
}

pull_articles(things)

# def main(things) 
# 	things2 = {}
# 	things.each {|k,v|
# 		file = File.open(v)
# 		file.each {|line| 
# 			things2

# 		}
# 		things2[k]

# 	}
# end