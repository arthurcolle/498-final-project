#!/usr/bin/env ruby

require 'time'
require 'date'

###############

# Sample program query:

# 	ruby percent_change.rb minutely_file.csv interval mm/dd/yyyy-hh:mm:00

# 	ruby percent_change.rb tkmr_minutely.csv 1440 09/21/2014-17:20:00

###############



filename = ARGV[0]
interval = ARGV[1]
strt = ARGV[2].gsub("-"," ")
puts "Querying " + filename + " for " + interval + " minute interval starting at " + strt

f = File.open(filename)	
name = filename.split(".")[0]
f = f.readlines

prices = {}

start = Time.strptime(f[1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i
en = Time.strptime(f[f.size-1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

requested = Time.strptime(strt, "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

ln_ct = 0
start.step(en, 60) do |i|
	prices[i] = f[ln_ct]
	ln_ct = ln_ct + 1
end

open_start = prices[requested].split(",")[1].to_f
open_end = prices[requested + (60*interval.to_i)].split(",")[1].to_f

puts "Price at start: " + open_start.to_s
puts "Price at end: " + open_end.to_s

puts (((open_end.to_f - open_start.to_f)/open_start.to_f)*100)

#for i in (0..f.size - 2) do
#	arg = f[i].split(",")
#        timestamp = arg[0]
#        open = arg[1]

	#puts start + " " + timestamp
#	if start == timestamp
#		arg1 = f[i + interval.to_i].split(",")
#                timestamp1 = arg1[0]
#                open1 = arg1[1]
#
#		percent_open = (((open1.to_f - open.to_f)/open.to_f)*100)
#	end
#end






