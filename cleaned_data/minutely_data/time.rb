require 'date'
require 'time'

#x = ["gold.csv", "silver.csv", "nge.csv", "dji.csv", "spy.csv", "tkmr.csv", "eza.csv", "USDJPY.csv", "EURUSD.csv", "GBPJPY.csv"]
#y = ["gold_minutely.csv", "silver_minutely.csv", "nge_minutely.csv", "dji_minutely.csv", "spy_minutely.csv", "tkmr_minutely.csv", "eza_minutely.csv", "USDJPY_minutely.csv", "EURUSD_minutely.csv", "GBPJPY_minutely.csv"]


x = ["USDJPY.csv", "EURUSD.csv", "GBPJPY.csv"]
y = ["USDJPY_minutely.csv", "EURUSD_minutely.csv", "GBPJPY_minutely.csv"]

for j in (0..x.size - 1) do

	filename = x[j]
        puts filename

        f = File.open(filename)
        f2 = File.open(y[j], "w")

	f = f.readlines
	prices = {}
	line_ct = 0

	f2.puts "Time,Open"
	
	start = Time.strptime(f[0].split(",")[0], "%Y/%m/%d %H:%M").strftime("%s").to_i
	en = Time.strptime(f[f.size-1].split(",")[0], "%Y/%m/%d %H:%M").strftime("%s").to_i
	
	start.step(en, 60) do |i|
		prices[i] = nil
	end


	f.each { |line|
		ln = line.split(",")
		if (ln[0].split(" ").size > 1)
			unix_time = Time.strptime(ln[0], "%Y/%m/%d %H:%M").strftime("%s").to_i
		else
			unix_time = Time.strptime(ln[0], "%Y/%m/%d").strftime("%s").to_i
		end
		prices[unix_time] = ln[1]
		line_ct = line_ct + 1
	}

	prices.each { |p,v|
		if v == nil
			prices[p] = prices[p - 60]
		end
	}
	prices.each { |p,v|
		f2.puts Time.at(p.to_i).strftime("%m/%d/%Y %H:%M:%S") + "," + v
	}

end
