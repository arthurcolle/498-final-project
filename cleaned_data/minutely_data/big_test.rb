#!/usr/bin/env ruby

require 'curb'
require 'json'
require 'time'
require 'date'

def main(things)
	
	t_hash = {}
	things.each { |k,v|		
		f = File.open(v)	
		name = v.split(".")[0]
		f = f.readlines
		prices = {}

		start = Time.strptime(f[1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i
		en = Time.strptime(f[f.size-1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

		ln_ct = 0
		start.step(en, 60) do |i|
			prices[i] = f[ln_ct]
			ln_ct = ln_ct + 1
		end
		t_hash[v] = prices

	}
	pull_articles(things, t_hash)
end

def pull_articles(things, t_hash)

	i = 1
	total = 10436
	while (i < total)
		http = Curl.get('localhost:9200/news/all2/'+i.to_s)
		body_str = http.body_str
		json = JSON.parse(body_str)

		source = json['_source']
		data = source['data']
		
		timestamp = data['timestamp']
		
		body = data['body']
		url = data['url']
		score = data['score']
		sentiment = data['sentiment']

		j = {}
		j[:data] = {}
		pc = j[:data][:percent_change] = {}
		
		things.each { |k,v| 
			pc[k] = {} if pc[k].nil?
			pc[k][:mins_1] = compute_change(v, 1, timestamp, t_hash)
			pc[k][:mins_5] = compute_change(v, 5, timestamp, t_hash)
			pc[k][:mins_30] = compute_change(v, 30, timestamp, t_hash)
			pc[k][:hours_1] = compute_change(v, 1*60, timestamp, t_hash)
			pc[k][:hours_2] = compute_change(v, 2*60, timestamp, t_hash)
			pc[k][:hours_3] = compute_change(v, 3*60, timestamp, t_hash)
			pc[k][:hours_6] = compute_change(v, 6*60, timestamp, t_hash)
			pc[k][:hours_12] = compute_change(v, 12*60, timestamp, t_hash)
			pc[k][:hours_24] = compute_change(v, 24*60, timestamp, t_hash)
		}
		g = JSON.generate(j)

		http = Curl.post('localhost:9200/news/pc/'+i.to_s, g)
		puts http.body_str
		i += 1
	end
end

def compute_change(filename, interval_in_minutes, date, t_hash)
	interval = interval_in_minutes
	strt = date.gsub("-"," ")
	#puts "Querying " + filename + " for " + interval.to_s + " minute interval starting at " + strt

	requested = Time.strptime(strt, "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

	open_start =  t_hash[filename][requested]
	open_end = t_hash[filename][requested + (60*interval.to_i)]

	if open_start == nil
		puts filename
		puts requested
	else 
		open_start = open_start.split(",")[1].to_f
	end

	if open_end == nil
		puts requested
	else
		open_end = open_end.split(",")[1].to_f
	end
	# puts "Price at start: " + open_start.to_s
	# puts "Price at end: " + open_end.to_s
	(((open_end.to_f - open_start.to_f)/open_start.to_f)*100)
end

things = {
	:eurodollar => "EURUSD_minutely.csv",
	:eza => "eza_minutely.csv",
	:poundyen => "GBPJPY_minutely.csv",
	:gold => "gold_minutely.csv",
	:nge => "nge_minutely.csv",
	:silver => "silver_minutely.csv",
	:spy => "spy_minutely.csv",
	:tmkr => "tkmr_minutely.csv",
	:dollaryen => "USDJPY_minutely.csv"
}


main(things)
