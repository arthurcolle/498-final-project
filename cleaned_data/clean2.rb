#!/usr/bin/env ruby

x = ["GBPJPY.csv", "USDJPY.csv", "EURUSD.csv"]
y = ["GBPJPY_percent.csv", "USDJPY_percent.csv", "EURUSD_percent.csv"]


for j in (0..2)

	filename = x[j]
	puts filename
 
	f = File.open(filename)	
	f2 = File.open(y[j], "w")
	name = filename.split(".")[0]

	f = f.readlines
	for i in (0..f.size - 2)

		arg = f[i].split(",")
		timestamp = arg[0]
		open = arg[1]
		high = arg[2]
		low = arg[3]
		close = arg[4].chomp
		
		arg1 = f[i + 1].split(",")
		timestamp1 = arg1[0]
		open1 = arg1[1]
		high1 = arg1[2]
		low1 = arg1[3]
		close1 = arg1[4].chomp
	
		f2.print timestamp
		f2.print ","
		percent_open = (((open1.to_f - open.to_f)/open.to_f)*100)
		f2.print percent_open
		
		f2.print ","
		percent_high = (((high1.to_f - high.to_f)/high.to_f)*100)
		f2.print percent_high

		f2.print ","
		percent_low = (((low1.to_f - low.to_f)/low.to_f)*100)
		f2.print percent_low
		
		f2.print ","
		percent_close = (((close1.to_f - close.to_f)/close.to_f)*100)
		f2.print percent_close
		f2.puts
	end

end
