#!/usr/bin/env ruby

require 'curb'
require 'json'
require 'sentimental'


i = 1
while (i <= 1338)
	url = Curl.get("localhost:9200/news/articles/"+i.to_s)
	body = url.body_str
	json_hash = JSON.parse(body)
	timestamp = json_hash['_source']['data']['timestamp']
	url = json_hash['_source']['data']['url']
	body = json_hash['_source']['data']['body']

	json = Hash.new
	json[:data] = Hash.new
	json_up = json[:data]
	json_up[:timestamp] = timestamp
	json_up[:url] = url
	json_up[:body] = body
	sentiment1 = analyzer.get_sentiment(body)
	score1 = analyzer.get_score(body)
	json_up[:sentiment] = sentiment1
	json_up[:score] = score1

	generated = JSON.generate(json_up)
	data = Hash.new
	data[:data] = json_up
	gener = JSON.generate(data)
	http = Curl.post("localhost:9200/news/sentiments/"+i.to_s, gener)
	puts http.body_str	
	
	i += 1
end

