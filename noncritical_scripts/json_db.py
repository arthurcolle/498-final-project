import cPickle as pickle
import json

#print "Loading pickle files"

dic1 = pickle.load(open("e1.pkl", "rb"))
dic2 = pickle.load(open("e2.pkl", "rb"))
dic3 = pickle.load(open("e3.pkl", "rb"))
dic4 = pickle.load(open("e4.pkl", "rb"))
dic5 = pickle.load(open("e5.pkl", "rb"))
dic6 = pickle.load(open("e6.pkl", "rb"))
dic7 = pickle.load(open("e7.pkl", "rb"))

#print "Making large dic"

dic = dict(dic1.items() + dic2.items() + dic3.items() + dic4.items() + dic5.items() + dic6.items() + dic7.items())

#print "Dumping file"

print(json.dumps(dic))