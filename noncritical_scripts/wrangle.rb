#!/usr/bin/env ruby

require 'curb'
require 'json'

sym = ('localhost:9200/prices/EURUSD/').to_sym
i = 1
j = 684110
while i <= j
	eur = Curl.get(sym.to_s+i.to_s)
	json = JSON.parse(eur.body_str)
	data = json['_source']['data']
	puts data['timestamp'] + "," + i.to_s
	i += 1
end
