#!/usr/bin/env ruby

require 'curb'
require 'json'

def pull_articles
	i = 1
	total = 11453
	while (i < total)
		i = 10000
		http = Curl.get('localhost:9200/news/all/'+i.to_s)
		body_str = http.body_str
		json = JSON.parse(body_str)

		source = json['_source']
		data = source['data']
		
		timestamp = data['timestamp']

		puts timestamp
		
		
		body = data['body']
		url = data['url']
		score = data['score']
		sentiment = data['sentiment']

		j = {}
		j[:data] = {}
		pc = j[:data][:percent_change] = {}
		
		things = {
			:eurodollar => "EURUSD_minutely.csv",
			:eza => "eza_minutely.csv",
			:poundyen => "GBPJPY_minutely.csv",
			:gold => "gold_minutely.csv",
			:nge => "nge_minutely.csv",
			:silver => "silver_minutely.csv",
			:spy => "spy_minutely.csv",
			:tmkr => "tkmr_minutely.csv",
			:dollaryen => "USDJPY_minutely.csv"
		}

		things.each { |k,v| 
			pc[k][:hours_1] = compute_change(v, 60, timestamp)
			pc[k][:hours_2] = compute_change(v, 2*60, timestamp)
			pc[k][:hours_6] = compute_change(v, 6*60, timestamp)
			pc[k][:hours_12] = compute_change(v, 12*60, timestsamp)
			pc[k][:hours_24] = compute_change(v, 24*60, timestamp)
		}
		i += 1
		puts pc
		break
	end
end

def compute_change(filename, interval_in_minutes, date)
	min = interval_in_minutes

	strt = date.gsub("-"," ")
	puts "Querying " + filename + " for " + min + " minute interval starting at " + strt

	f = File.open(filename)	
	name = filename.split(".")[0]
	f = f.readlines

	prices = {}

	start = Time.strptime(f[1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i
	en = Time.strptime(f[f.size-1].split(",")[0], "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

	requested = Time.strptime(strt, "%m/%d/%Y %H:%M:%S").strftime("%s").to_i

	ln_ct = 0
	start.step(en, 60) do |i|
		prices[i] = f[ln_ct]
		ln_ct = ln_ct + 1
	end

	open_start = prices[requested].split(",")[1].to_f
	open_end = prices[requested + (60*interval.to_i)].split(",")[1].to_f

	# puts "Price at start: " + open_start.to_s
	# puts "Price at end: " + open_end.to_s
	
	
	(((open_end.to_f - open_start.to_f)/open_start.to_f)*100)
end

pull_articles